from datetime import date

import plotly as py
import plotly.graph_objects as go
from pymongo import MongoClient


def make_cluster():
    try:
        # Mudar password
        cluster = MongoClient('mongodb+srv://test:lets@cluster0-ystvx.gcp.mongodb.net/test?retryWrites=true&w=majority')
        return cluster
    except Exception as error:
        print(error)


def retrieve_collection(cluster, year_input):
    try:
        year = 'tx_' + year_input
        # mudar cluster e collection nomes
        db = cluster['transactions']
        collection = db[year]
        return collection
    except Exception as error:
        print(error)


def query_equal_gte_lte(field, operator, query, collection):
    results = collection.find({field: {operator: query}})
    return results


def plot_graph(collection, year_to_see):
    count_knowable = [0] * 365
    count_not_knowable = [0] * 365
    d0 = date(year_to_see, 1, 1)

    for document in collection.find({'year': year_to_see}):
        print(document)
        day = document['day']
        month = document['month']
        year = document['year']

        d1 = date(year, month, day)
        delta = d1 - d0

        days_past = delta.days

        if document['protocol'] != 'Not knowable':
            count_knowable[days_past] += 1
        else:
            count_not_knowable[days_past] += 1

    x = list(range(1, 366))
    y_know = count_knowable
    y_not_know = count_not_knowable

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=x,
        y=y_know,
        name="Knowable"
    ))

    fig.add_trace(go.Scatter(
        x=x,
        y=y_not_know,
        name="Not knowable"
    ))

    fig.update_layout(
        title="{} transactions that used OP_RETURNS".format(year),
        xaxis_title="Days",
        yaxis_title="Occurrences",
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="#7f7f7f"
        )
    )

    fig.show()


def query_count(field, query, collection):
    count_result = collection.find({field: {'$eq': query}}).count()
    return count_result


def sum_values(collection, year_to_see):
    # if year_to_see != 2016:
    #     values_knowable = [0] * 365
    #     values_unknowable = [0] * 365
    #     x = list(range(1, 366))
    # else:
    #     values_knowable = [0] * 366
    #     values_unknowable = [0] * 366
    #     x = list(range(1, 367))
    values_knowable = [0] * 365
    values_unknowable = [0] * 365
    for document in collection.find({'year': year_to_see}):
        size_receivers = len(document['addresses_receivers'])
        if len(document['addresses_receivers']) != 0:
            values_output = document['values_outputs']
            day = document['day']
            month = document['month']
            year = document['year']
            d0 = date(year_to_see, 1, 1)
            d1 = date(year, month, day)
            delta = d1 - d0
            days_past = delta.days
            if document['addresses_receivers'][size_receivers - 1] in document['addresses_senders']:
                values_output.pop(size_receivers - 1)
            for value_output in values_output:
                value_output = float(str(value_output))
                if document['protocol'] != 'Not knowable':
                    values_knowable[days_past] += value_output
                else:
                    values_unknowable[days_past] += value_output

    x = list(range(1, 366))
    y_values_knowable = values_knowable
    y_values_unknowable = values_unknowable

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=x,
        y=y_values_knowable,
        name="Knowable"
    ))

    fig.add_trace(go.Scatter(
        x=x,
        y=y_values_unknowable,
        name="Unknowable"
    ))

    fig.update_layout(
        title="{} transactions values that used OP_RETURNS".format(year_to_see),
        xaxis_title="Days",
        yaxis_title="Values",
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="#7f7f7f"
        )
    )

    fig.write_image("fig1.png")
    #fig.show()


def menu():
    print('------------------------------------------')
    print('Query menu:\n'
          '1-Query: transaction id;\n'
          '2-Query: address sender;\n'
          '3-Query: address receiver;\n'
          '4-Query: protocols used;\n'
          '5-Query: bitcoins spend (greater or equal);\n'
          '6-Query: bitcoins spend (less or equal);\n'
          '7-Query: count address sender;\n'
          '8-Query: count address receiver;\n'
          '9-Query: count protocol;\n'
          '10-Query: count all knowable and not knowable protocols;\n'
          '11-Query: count all transactions;\n'
          '12-Plot histogram of knowable and not knowable protocols (quantities/day);\n'
          '13-Plot histogram of sum of values (value sum/day)'
          '14-Plot histogram of average sum of values (average/day)'
          '15-Exit menu.')
    print('------------------------------------------')


def main():
    results = {}
    cluster = make_cluster()
    if cluster:
        while True:
            menu()
            input_option = int(input('Option: '))
            if input_option == 15:
                year = int(input('From what year to search?'))
                collection = retrieve_collection(cluster, str(year))
                sum_values(collection, year)
            elif input_option == 16:
                print('Exiting...')
                break
            print()
            input('Press Enter to continue...')


main()
