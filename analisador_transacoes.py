import json
import urllib.request

import blockchain


TRANSACOES_CONHECIDAS = {}
TRANSACOES_DESCONHECIDAS = {}

#########################################################33https://www.google.com/search?client=firefox-b-d&ei=yJpAXsHYK-GG0AbKwJegDg&q=unknowable+definition&oq=unknowable+definition&gs_l=psy-ab.3..0i19j0i30i19l3j0i5i30i19j0i8i10i30i19j0i8i30i19l4.12023.12193..13213...1.0..0.218.489.0j2j1......0....1..gws-wiz.......0i13i30i19j0i8i13i10i30i19j0i8i13i30i19.iaD0jY8K_Uc&ved=0ahUKEwjB3MTO1MXnAhVhA9QKHUrgBeQQ4dUDCAo&uact=5

#**********olhar 141 do bloco 556471



###############################################################3

def requisicao(url):
    try:
        req = urllib.request.urlopen(url)
        return req
    except Exception as e:
        print(e)
        exit()


def parsing(req):
    json_str = req.read()
    encoding = req.info().get_content_charset('utf-8')
    dicionario = json.loads(json_str.decode(encoding))
    return dicionario


def filtra_transacao_extraida(dicionario):
    for op_return in range(len(dicionario['op_returns'])):
        transacao_id = dicionario['op_returns'][op_return]['txid']
        script = dicionario['op_returns'][op_return]['script']
        hex_t = dicionario['op_returns'][op_return]['hex']
        ascii_t = dicionario['op_returns'][op_return]['ascii']
        if len(dicionario['op_returns'][op_return]['protocols']) == 0:
            insere_transacoes_desconhecidas(transacao_id, script, hex_t, ascii_t)
        else:
            protocols = dicionario['op_returns'][op_return]['protocols'][0]['name']
            insere_transacoes_conhecidas(transacao_id, script, hex_t, ascii_t, protocols)


def insere_transacoes_conhecidas(transacao_id, script, hex_t, ascii_t, protocols):
    TRANSACOES_CONHECIDAS[transacao_id] = {
        'script': script,
        'hex': hex_t,
        'ascii': ascii_t,
        'protocols': protocols,
    }


def insere_transacoes_desconhecidas(transacao_id, script, hex_t, ascii_t):
    TRANSACOES_DESCONHECIDAS[transacao_id] = {
        'script': script,
        'hex': hex_t,
        'ascii': ascii_t,
    }


def salvar(block_height, protocolo_status, transacoes):
    try:
        with open('bloco_' + str(block_height) + '_' + protocolo_status, 'w') as arquivo:
            for transacao in transacoes:
                script = transacoes[transacao]['script']
                hex_t = transacoes[transacao]['hex']
                ascii_t = transacoes[transacao]['ascii']
                if transacoes == TRANSACOES_CONHECIDAS:
                    protocol_t = transacoes[transacao]['protocols']
                    arquivo.write('{},{},{},{},{}\n'.format(transacao, script, hex_t, protocol_t, ascii_t))
                else:
                    arquivo.write('{},{},{},{}\n'.format(transacao, script, hex_t, ascii_t))
    except Exception as error:
        print(error)




def main():
    block_height = 556471#int(input('Bloco inicial: '))
    block_end = 556471#int(input('Bloco final: '))
                            #319997#556471
                            #319998#556471
    while block_height <= block_end:
        url = 'http://api.coinsecrets.org/block/'+str(block_height)
        resposta = requisicao(url)
        dicionario = parsing(resposta)
        if len(dicionario['op_returns']) != 0:
            filtra_transacao_extraida(dicionario)
            salvar(block_height, 'conhecidos', TRANSACOES_CONHECIDAS)
            salvar(block_height, 'desconhecidos', TRANSACOES_DESCONHECIDAS)
            print('Dados do bloco {} salvos com sucesso!'.format(block_height))

        block_height += 1
        TRANSACOES_CONHECIDAS.clear()
        TRANSACOES_DESCONHECIDAS.clear()


main()