import re

import plotly.graph_objects as go
import pymongo
from pymongo import MongoClient


def make_cluster():
    try:
        # Mudar password
        cluster = MongoClient('mongodb+srv://test:lets@cluster0-ystvx.gcp.mongodb.net/test?retryWrites=true&w=majority')
        return cluster
    except Exception as error:
        print(error)


def retrieve_collection(cluster):
    try:
        # mudar cluster e collection nomes
        db = cluster['test']
        collection = db['test']
        return collection
    except Exception as error:
        print(error)


def query_equal_gte_lte(field, operator, query, collection):
    results = collection.find({field: {operator: query}})
    return results


def query_gte(query, collection):
    pass


def query_lte(query, collection):
    pass


def plot_graph(collection):
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    count_knowable = [0] * 12
    count_not_knowable = [0] * 12

    for document in collection.find({'month': 9}):
        print(document)
        month = document['month']
        if document['protocol'] != 'Not knowable':
            count_knowable[(month - 1)] += 1
        else:
            count_not_knowable[(month - 1)] += 1
    print(count_knowable)
    print(count_not_knowable)
    print(months)

    x = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    y_know = count_knowable#[5, 10, 3, 4, 100, 5, 80, 41, 23, 47, 55, 42]
    y_not_know = count_not_knowable#[10, 10, 10, 75, 96, 62, 130, 44, 1, 99, 8, 5]

    fig = go.Figure()
    fig.update_layout(
        title='2019 transactions that used OP_RETURNS',
        xaxis_title="Months",
        yaxis_title="Quantities",
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="#7f7f7f"
        )
    )
    # fig.add_trace(go.Histogram(histfunc="count", y=y, x=x, name="count"))
    # fig.add_trace(go.Histogram(histfunc="sum", y=y, x=x, name="sum"))
    fig.add_trace(go.Histogram(histfunc='sum', y=y_know, x=x, name='Knowable'))
    fig.add_trace(go.Histogram(histfunc='sum', y=y_not_know, x=x, name='Not knowable'))

    fig.show()



def query_count(field, query, collection):
    count_result = collection.find({field: {'$eq': query}}).count()
    return count_result


def menu():
    print('------------------------------------------')
    print('Query menu:\n'
          '1-Query: transaction id;\n'
          '2-Query: address sender;\n'
          '3-Query: address receiver;\n'
          '4-Query: protocols used;\n'
          '5-Query: bitcoins spend (greater or equal);\n'
          '6-Query: bitcoins spend (less or equal);\n'
          '7-Query: count address sender;\n'
          '8-Query: count address receiver;\n'
          '9-Query: count protocol;\n'
          '10-Query: count all knowable and not knowable protocols;\n'
          '11-Query: count all transactions;\n'
          '12-Plot histogram of knowable and not knowable protocols (quantities/month);\n'
          '13-Exit menu.')
    print('------------------------------------------')


def main():
    results = {}
    cluster = make_cluster()
    if cluster:
        collection = retrieve_collection(cluster)
        if collection:
            print('------------------------------------------')
            print('Connection with the database has been made')
            print('------------------------------------------')
            print()
        while True:
            menu()
            input_option = int(input('Option: '))
            if input_option == 1:
                tx_id = input('Enter a transaction id: ')
                results = query_equal_gte_lte('_id', '$eq', tx_id, collection)
                # results = collection.find({'_id': {'$eq': tx_id}})
            elif input_option == 2:
                address_sender = input('Enter the address sender: ')
                results = collection.find({'addresses_senders': {'$eq': address_sender}})
            elif input_option == 3:
                address_receiver = input('Enter the address receiver: ')
                results = collection.find({'addresses_receivers': {'$eq': address_receiver}})
            elif input_option == 4:
                protocol = input('Enter a protocol name: ')
                results = collection.find({'protocol': {'$eq': protocol}})
            elif input_option == 5:
                bitcoin_spend = float(input('Enter a value: '))
                results = collection.find({'values_inputs': {'$gte': bitcoin_spend}})
            elif input_option == 6:
                bitcoin_spend = input('Enter value: ')
                results = collection.find({'values_inputs': {'$lte': bitcoin_spend}})
            elif input_option == 7:
                address_sender = input('Enter the address sender for count: ')
                count_result = collection.find({'addresses_senders': {'$eq': address_sender}}).count()
                print('The address {} has appeared {} times.'.format(address_sender, count_result))
            elif input_option == 8:
                address_receiver = input('Enter the address receiver for count: ')
                count_result = collection.find({'addresses_receivers': {'$eq': address_receiver}}).count()
                print('The address {} has appeared {} times.'.format(address_receiver, count_result))
            elif input_option == 9:
                protocol = input('Enter a protocol name for count: ')
                count_result = collection.find({'protocol': {'$eq': protocol}}).count()
                print('The protocol {} has appeared {} times.'.format(protocol, count_result))
            elif input_option == 10:
                count_know = collection.count_documents({'protocol': {'$ne': 'Not knowable'}})
                count_not_know = collection.count_documents({'protocol': {'$eq': 'Not knowable'}})
                #count_not_know = collection.find({'protocol': {'$eq': 'Not knowable'}}).count()
                print('Knowable: {}\n'
                      'Not knowable: {}'.format(count_know, count_not_know))
            elif input_option == 11:
                count_all = collection.count_documents({})
                print('This database has a total of {} transactions'.format(count_all))
            elif input_option == 12:
                plot_graph(collection)
            elif input_option == 13:
                print('Exiting...')
                break
            for result in results:
                print(result)
            print()
            input('Press Enter to continue...')


main()

#results = {}
#cluster = make_cluster()

#months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
#count_knowable = [0] * 12
#count_not_knowable = [0] * 12

#if cluster:
#    myCollection = retrieve_collection(cluster)

#for document in myCollection.find({'month': 9}):
#    print(document)
#    month = document['month'] # iterate the cursor
#    if document['protocol'] != 'Not knowable':
#        count_knowable[(month-1)] += 1
#    else:
#        count_not_knowable[(month-1)] += 1
#print(count_knowable)
#print(count_not_knowable)
#print(months)
