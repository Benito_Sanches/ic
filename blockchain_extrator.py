import json
import time
from datetime import datetime

import requests
from bson import Decimal128
from pymongo import MongoClient

URL = 'https://blockchain.info/rawtx/'  # 16bd58ccc4cc0d00dc57d31878198a8fd5038b6774f5525917ec0b014ecc02f9'
IDS_TRANSACTIONS = []
TX_INFO_COINSECRETS = {}
TX_INFO_BLOCKCHAIN = {}


def request(url):
    try:
        request = requests.get(url)
        if request.status_code == 200:
            return request
        else:
            print(request.status_code)
            return None
    except Exception as e:
        print(e)
        return None


def parsing(request):
    try:
        dict = json.loads(request.text)
        return dict
    except Exception as error:
        print(error)
        return None


def make_cluster():
    try:
        # Mudar password
        cluster = MongoClient('mongodb+srv://test:lets@cluster0-ystvx.gcp.mongodb.net/test?retryWrites=true&w=majority')
        return cluster
    except Exception as error:
        print(error)


def retrieve_collection(cluster):
    try:
        #mudar cluster e collection nomes
        db = cluster['test']
        collection = db['test']
        return collection
    except Exception as error:
        print(error)


def extract_id_transaction(block_height, protocol_status):
    index = 0
    try:
        with open('bloco_' + block_height + '_' + protocol_status, 'r') as file:
            transactions_data = file.read()
            transactions_data = transactions_data.split('\n')
            for transaction_data in transactions_data:
                if len(transaction_data) > 0:
                    tx_id, tx_script, tx_hex, tx_protocol, tx_ascii = split_line(transaction_data, protocol_status)
                    insert_coinsecrets_infos(tx_id, tx_script, tx_hex, tx_protocol, tx_ascii, block_height, index)
                    IDS_TRANSACTIONS.append(tx_id)
                    index += 1
    except Exception as error:
        print('Error: {} in {}'.format(error, block_height))
        return None


def split_line(transaction_data, protocol_status):
    if protocol_status == 'conhecidos':
        tx_infos = transaction_data.split(',', 4)
        tx_id = tx_infos[0]
        tx_script = tx_infos[1]
        tx_hex = tx_infos[2]
        tx_protocol = tx_infos[3]
        tx_ascii = tx_infos[4]
    elif protocol_status == 'desconhecidos':
        tx_infos = transaction_data.split(',', 3)
        tx_id = tx_infos[0]
        tx_script = tx_infos[1]
        tx_hex = tx_infos[2]
        tx_ascii = tx_infos[3]
        tx_protocol = 'Not knowable'
    return tx_id, tx_script, tx_hex, tx_protocol, tx_ascii


def extract_transaction_info(dict):
    addresses_senders = []
    values_inputs = []
    values_outputs = []
    addresses_receivers = []

    inputs = dict['inputs']
    outputs = dict['out']

    try:
        for inp in inputs:
            if not inp['prev_out']['addr'] in addresses_senders:
                addresses_senders.append(inp['prev_out']['addr'])
            values_inputs.append(Decimal128('%.8f' % (inp['prev_out']['value'] * 10 ** -8)))
    except KeyError:
        addresses_senders = 'Newly_generated_coins'
    except Exception as error:
        print('Error in extract transaction info: {}'.format(error))
        return None

    for output in outputs:
        if output['value'] != 0:
            values_outputs.append(Decimal128('%.8f' % (output['value'] * 10 ** -8)))
            addresses_receivers.append(output['addr'])

    ts = int(dict['time'])
    month = datetime.utcfromtimestamp(ts).month
    print(month)

    return addresses_senders, values_inputs, addresses_receivers, values_outputs, month


def insert_transactions_blockchain(addresses_senders, values_inputs, addresses_receivers, values_outputs, month, index):
    TX_INFO_BLOCKCHAIN[index] = {
        'addresses_senders': addresses_senders,
        'value_input': values_inputs,
        'addresses_receivers': addresses_receivers,
        'values_outputs': values_outputs,
        'month': month,
    }


def insert_coinsecrets_infos(tx_id, tx_script, tx_hex, tx_protocol, tx_ascii, block_height, index):
    TX_INFO_COINSECRETS[index] = {
        'tx_id': tx_id,
        'tx_script': tx_script,
        'tx_hex': tx_hex,
        'tx_protocol': tx_protocol,
        'tx_ascii': tx_ascii,
        'block_height': block_height,
    }


def post_database(collection):
    for i in range(len(TX_INFO_COINSECRETS)):
        tx_id = TX_INFO_COINSECRETS[i]['tx_id']
        tx_script = TX_INFO_COINSECRETS[i]['tx_script']
        tx_hex = TX_INFO_COINSECRETS[i]['tx_hex']
        tx_protocol = TX_INFO_COINSECRETS[i]['tx_protocol']
        tx_ascii = TX_INFO_COINSECRETS[i]['tx_ascii']
        block_height = TX_INFO_COINSECRETS[i]['block_height']

        address_sender = TX_INFO_BLOCKCHAIN[i]['addresses_senders']
        values_inputs = TX_INFO_BLOCKCHAIN[i]['value_input']
        addresses_receivers = TX_INFO_BLOCKCHAIN[i]['addresses_receivers']
        values_outputs = TX_INFO_BLOCKCHAIN[i]['values_outputs']
        month = TX_INFO_BLOCKCHAIN[i]['month']

        post = {'_id': tx_id, 'script': tx_script, 'hex': tx_hex, 'protocol': tx_protocol, 'ascii': tx_ascii,
                'addresses_senders': address_sender, 'values_inputs': values_inputs,
                'addresses_receivers': addresses_receivers, 'values_outputs': values_outputs,
                'block_height': block_height, 'month': month}

        try:
            collection.insert_one(post)
            print('Transaction {} from the block {} has been uploaded in the server.'.format(tx_id, block_height))
        except Exception as error:
            print('Error in post: '.format(error))


def process_transaction(collection):
    count_sleep = 0
    index = 0
    for id_transaction in IDS_TRANSACTIONS:
        req = request(URL + id_transaction)
        if req:
            dict = parsing(req)
            if dict:
                address_sender, values_inputs, addresses_receivers, values_outputs = extract_transaction_info(dict)
                insert_transactions_blockchain(address_sender, values_inputs, addresses_receivers, values_outputs, index)
                count_sleep += 1
                index += 1
                if count_sleep == 150:
                    time.sleep(5)
    #post_database(collection)
    print(TX_INFO_BLOCKCHAIN)


def clear_all():
    IDS_TRANSACTIONS.clear()
    TX_INFO_COINSECRETS.clear()
    TX_INFO_BLOCKCHAIN.clear()


def main():
    count_sleep = 0
    index = 0

    block_ini = 319998#556471  # int(input('Initial block: '))
    block_end = 319998#556471  # int(input('Last block: '))

    protocol_status = ['conhecidos', 'desconhecidos']

    block_height = block_ini  # 319997#556471

    cluster = make_cluster()
    collection = retrieve_collection(cluster)

    while block_height <= block_end:
        for status in protocol_status:
            extract_id_transaction(str(block_height), status)
            for id_transaction in IDS_TRANSACTIONS:
                req = request(URL + id_transaction)
                if req:
                    dict = parsing(req)
                    if dict:
                        address_sender, values_inputs, addresses_receivers, values_outputs, month = extract_transaction_info(dict)
                        insert_transactions_blockchain(address_sender, values_inputs, addresses_receivers,
                                                       values_outputs, month, index)
                        count_sleep += 1
                        index += 1
                        if count_sleep == 250:
                            time.sleep(5)
                            count_sleep = 0
            post_database(collection)
            index = 0
            clear_all()
        block_height += 1


main()
